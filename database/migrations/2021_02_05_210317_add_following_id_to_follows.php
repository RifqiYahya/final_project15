<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFollowingIdToFollows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('follows', function (Blueprint $table) {
          $table->unsignedBigInteger('following_id');
          $table->foreign('following_id')->references('id')->on('users')->onDelete('cascade');
          $table->unsignedBigInteger('followed_id');
          $table->foreign('followed_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('follows', function (Blueprint $table) {
          $table->dropForeign(['following_id']);
          $table->dropColumn(['following_id']);
          $table->dropForeign(['followed_id']);
          $table->dropColumn(['followed_id']);
        });
    }
}
