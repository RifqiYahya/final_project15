<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPostIdToPostLikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_likes', function (Blueprint $table) {
          $table->unsignedBigInteger('post_id');
          $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
          $table->unsignedBigInteger('user_id');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_likes', function (Blueprint $table) {
          $table->dropForeign(['post_id']);
          $table->dropColumn(['post_id']);
          $table->dropForeign(['user_id']);
          $table->dropColumn(['user_id']);
        });
    }
}
