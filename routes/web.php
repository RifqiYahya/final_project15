<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use Illuminate\Http\Request;

Route::get('/', function () {
    if(Auth::check()){
        return redirect('/home');
    }
    return redirect('/login');
});

//route search
Route::get('/search', function (Request $request) {
    $results = User::where('name', 'like', '%'.$request->keyword.'%')->get();
    // dd($results);
    return view ('search', compact('results'));
});

Route::resource('posts', 'PostController');
Route::post('posts/{id}/like', 'PostController@like');
Route::post('posts/{id}/unlike', 'PostController@unlike');

Route::resource('users', 'UserController');

Route::resource('comments', 'CommentController');
Route::post('comments/{id}/like', 'CommentController@like');
Route::post('comments/{id}/unlike', 'CommentController@unlike');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile/edit','ProfileController@edit');

Route::get('/profile','ProfileController@index');
Route::put('/profile','ProfileController@update');

Route::get('/profile/{id}','ProfileController@show');
Route::delete('/profile/{id}','ProfileController@delete');

Route::put('/profile/change_password','ProfileController@change_password');
Route::get('/profile/verify_email','ProfileController@verify_email');
route::post('/profile/{id}/follow', 'ProfileController@follow');
route::post('/profile/{id}/unfollow', 'ProfileController@unfollow');