<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment_like extends Model
{
  protected $table = "comment_likes";

  public $timestamps = false;

  protected $guarded = [];
}
