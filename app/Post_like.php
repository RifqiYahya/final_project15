<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post_like extends Model
{
  protected $table = "post_likes";
  public $timestamps = false;

  protected $guarded = [];
}
