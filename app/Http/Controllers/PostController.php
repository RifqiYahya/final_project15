<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Post_like;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required',
        ]);

        if ($request->hasFile('picture')){
            $request->validate([
                'picture' => 'file|image|max:5000'
            ]);
        }

        $post = Post::create([
          "isi" => $request->isi,
          "user_id" => Auth::user()->id,
          "picture" => isset($request->picture) ? $request->picture->store('post_pics', 'public') : null
        ]);

        return redirect('/home')->with('success', 'Post Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $request->validate([
            'isi' => 'required',
        ]);

        if ($request->hasFile('picture')){
            $request->validate([
                'picture' => 'file|image|max:5000'
            ]);
        }

        $update = Post::where('id', $id)->update([
          "isi" => $request->isi,
        ]);
        
        if ($request->picture){
            if ($post->picture){
                Storage::delete('public/'.$post->picture);
            }

            $update = Post::where('id', $id)->update([
                "picture" => $request->picture->store('post_pics', 'public')
            ]);
        }

        return redirect('/posts')->with('success', 'Berhasil update post!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if (Storage::exists('public/'.$post->picture)){
            Storage::delete('public/'.$post->picture);
        } else {
            dd(Storage::files('File not found'));
        }

        Post::destroy($id);

        return redirect('/home')->with('success', 'Post berhasil dihapus!');
    }

    // Like function
    public function like($id){
        $like = Post_like::create([
            'point' => 1,
            'user_id' => Auth::user()->id,
            'post_id' => $id
        ]);

        return redirect('/home');
    }

    //Unlike function
    public function unlike($id){
        Post_like::where([
            'post_id' => $id,
            'user_id' => Auth::user()->id
        ])->delete();

        return redirect('/home');
    }
}
