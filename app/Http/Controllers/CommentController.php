<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Comment_like;
use Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required'
        ]);

        $comment = Comment::create([
            "isi" => $request->isi,
            "user_id" => Auth::user()->id,
            "post_id" => $request->post_id
        ]);

        return redirect("/posts/{$comment->post_id}");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::find($id);
        return redirect("/posts/{$comment->post_id}");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        Comment::destroy($id);

        return redirect("/posts/{$request->post_id}");
    }

    //like function
    public function like($id, Request $request) {
        $like = Comment_like::create([
            'point' => 1,
            'user_id' => Auth::user()->id,
            'comment_id' => $id
        ]);

        return redirect("/posts/{$request->post_id}");
    }

    //unlike function
    public function unlike($id, Request $request){
        Comment_like::where([
            'comment_id' => $id,
            'user_id' => Auth::user()->id
        ])->delete();

        return redirect("/posts/{$request->post_id}");
    }
}
