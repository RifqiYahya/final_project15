<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Follow;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::where(function($query){
            $followings = Follow::where('following_id', Auth::user()->id)->get();
            // dd($followings);
            foreach ($followings as $following){
                $query->orwhere('user_id', $following->followed_id);
            }
            $query->orwhere('user_id', Auth::user()->id);
        })->orderBy('updated_at', 'desc')->get();
        // dd($posts);

        return view('home', compact('posts'));
    }
}
