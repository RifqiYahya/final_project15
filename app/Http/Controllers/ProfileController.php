<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Post;
use App\User;
use App\Follow;

class ProfileController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');
    } 
    
    public function index(){
        $id = Auth::user()->id;
        $posts = Post::where('user_id',$id)->orderBy('id','DESC')->get();
        $profile = User::find(Auth::user()->id);
        return view('profile.index', compact('id','profile','posts'));
    }

    public function show($id){
        $posts = Post::where('user_id',$id)->orderBy('id','DESC')->get();
        $profile = User::find($id);
        $followed = Follow::where([
            'following_id' => Auth::user()->id,
            'followed_id' => $id
        ])->get();
        return view('profile.index', compact('id','profile','posts', 'followed'));
    }

    public function edit(){
        $id = Auth::user()->id;
        $profile = DB::table('users')->find($id);
        return view('profile.edit', compact('id','profile'));
    }

    public function update(Request $request){
        
        $id = Auth::user()->id;
        $user = User::find($id);
        $update = User::find($id)->update([
            "name" => $request["name"],
            "email" => $request["email"],
            "bio" => $request['bio'],
            "gender" => $request['gender']
        ]);

        if ($request->hasFile('picture')){
            $request->validate([
                'picture' => 'file|image|max:5000'
            ]);
        }

        if ($user->picture){
            if ($request->picture){
                Storage::delete('public/'.$user->picture);

                $update = User::find($id)->update([
                    "picture" => $request->picture->store('profile_pics', 'public')
                ]);
            }
        } else {
            $update = User::find($id)->update([
                "picture" => $request->picture->store('profile_pics', 'public')
            ]);
        }
  
        return redirect('/profile')->with('success', 'Berhasil update profile!');
    }

    public function change_password(Request $request){
        $id = Auth::user()->id;
        $password_lama = DB::table('users')->find($id)->password;
        if ($request["password"]!=$request["confirm_password"]){
            return redirect()->action('ProfileController@edit')->withErrors(['Konfirmasi password salah']);
        } else if (Hash::make($request["password_lama"])!=$password_lama){
            return redirect()->action('ProfileController@edit')->withErrors(['Password lama salah']);
        } else {
            $update = DB::table('users')->where('id',$id)->update([
                "password" => Hash::make($request["password"])
            ]);

            return redirect('/profile')->with('success', 'Berhasil ganti password');
        }
    }

    public function verify_email(){
        // kirim email dst
        // tidak dibuat
        return redirect()->action('ProfileController@edit');
    }

    public function delete(Request $request, $id){
        if ($request["konfirmasi"]==Auth::user()->name){
            DB::table('users')->where('id',$id)->delete();
            return redirect('/login')->with('success', 'Berhasil hapus akun');
        } else {
            return redirect()->action('ProfileController@edit')->withErrors(['Konfirmasi salah']);
        }
    }

    // Fungsi untuk follow
    public function follow($id){
        Follow::create([
            'following_id' => Auth::user()->id,
            'followed_id' => $id
        ]);

        return redirect("/profile/{$id}");
    }

    // fungsi untuk unfollow
    public function unfollow($id){
        Follow::where([
            'following_id' => Auth::user()->id,
            'followed_id' => $id
        ])->delete();

        return redirect("/profile/{$id}");
    }
}
