@extends('layouts.master')

@section('webtitle', 'MySocmed | Home')

@section('content')
    <div class="row">
        <div class="col-8">
            <div class="card p-2">
                <form action="/posts" method="POST" enctype="multipart/form-data">
                    @csrf
                      <div class="card-body p-0">
                        <div class="form-group mb-1">
                            <textarea rows="3" name="isi" id="isi" class="form-control" placeholder="What's on Your Mind?"></textarea>
                        </div>
                        <div style="color: red; font-size:12px;">{{ $errors->first('isi') }}</div>
                        <hr class="my-2">
                      </div>
                      <!-- /.card-body -->
                      <div class="card- footer d-flex justify-content-between">
                        <div>
                          <span class="mx-2"><i class="fas fa-image"></i> &nbsp; Add image : </span>
                          <input type="file" name="picture" id="picture">
                          {{-- <button id="imgbutton" class="opt btn btn-light btn-sm ml-1">Add picture &nbsp;<i class="fa fa-image"></i></button> --}}
                          {{-- <button class="opt btn btn-light btn-sm">Quotes &nbsp;  <i class="fas fa-quote-right"></i></button> --}}
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">Post &nbsp;<i class="fas fa-paper-plane pt-1 mr-1"></i></button>
                      </div>
                      <div style="color: red; font-size:12px;">{{ $errors->first('picture') }}</div>
                </form>
                  <!-- /.card-footer-->
            </div>
            <hr>

            {{-- contoh postingan --}}
            @forelse ($posts as $post)    
              @component('layouts.partials.postcard')
                @slot('post', $post)
              @endcomponent
            @empty

            @endforelse
        </div>
        <div class="col-4">
            @include('layouts.partials.dongle')
        </div>
    </div>
  
@endsection
