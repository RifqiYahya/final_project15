@extends('layouts.master')

@section('webtitle', 'My Socmed | Edit Profile')

@section('content')
    <div class="row">
        <div class="col-8">
            <div class="card p-2">
                <div class="card-header p-3">
                    <div class="container d-flex align-items-center">
                        <h2>Edit Profile</h2>
                    </div>
                </div>
                <div class="card-body p-4 m-2">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <h4>Identitas</h4>
                    <form action="/profile" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <label for="name" style="width:30%">Nama</label>:
                        <input type="text" id="name" name="name" value="{{$profile->name}}" style="width:65%" required><br>
                        <label for="email" style="width:30%">Email</label>:
                        <input type="text" id="email" name="email" value="{{$profile->email}}" style="width:65%" required><br>
                        <label for="bio" style="width:30%">Bio</label>:
                        <input type="text" id="bio" name="bio" value="{{$profile->bio}}" style="width:65%"><br>
                        <label for="gender" style="width:30%">Gender</label>:
                        <select name="gender" id="genderselect">
                            <option value="0">Laki-laki</option>
                            <option value="1" >Perempuan</option>
                        </select><br>
                        <label for="Profile Picture" style="width:30%">Profile Picture</label>:
                        <input type="file" name="picture"><br>
                        
                        
                        @if($profile->email_verified_at==NULL)
                        <a href="/profile/verify_email">Verify Email</a>
                        @endif

                        <br><br>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                   

                    <hr>

                    <h4>Change Password</h4>
                    <form action="/profile/change_password" method="POST">
                        @csrf
                        @method('PUT')
                        <label for="password_lama" style="width:30%">Password Lama</label>:
                        <input type="password" id="password_lama" name="password_lama" style="width:65%" required><br>
                        <label for="password" style="width:30%">Password</label>:
                        <input type="password" id="password" name="password" style="width:65%" required><br>
                        <label for="confirm_password" style="width:30%">Konfirmasi Password</label>:
                        <input type="password" id="confirm_password" name="confirm_password" style="width:65%" required><br>
                        <button type="submit" class="btn btn-dark">Change Password</button>
                    </form>

                    <hr>
                    
                    <h4>Hapus akun</h4>
                        <form action="/profile/{{$id}}" method="POST" style="display:inline">
                            @csrf
                            @method('DELETE')
                            <label for="konfirmasi">Silakan ketik nama lengkap anda.</label><br>
                            <input type="text" id="konfirmasi" name="konfirmasi" style="width:95%" required><br>
                            <input type="submit" class="btn btn-danger my-1" value="Delete Akun">
                        </form>
                    </div>
            </div>
        </div>
        <div class="col-4">
            @include('layouts.partials.dongle')
        </div>
    </div>
@endsection

@php
    $_gender = $profile->gender;
@endphp

@push('scripts')
    <script>
        const gender = document.getElementById('genderselect');
        let i = {{$_gender}}

        if (i >= 0) {
            gender.selectedIndex = i;
        } else {
            gender.selectedIndex = -1;
        }
        
    </script>
@endpush