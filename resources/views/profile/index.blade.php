@extends('layouts.master')

@section('webtitle', 'My Socmed | Profile')

@section('content')
    <div class="row">
        <div class="col-8">
            <div class="card p-2">
                <div class="card-header">
                    <div class="container d-flex align-items-center">
                        <img class='rounded-circle' 
                        style="width: 75px; height:75px; object-fit:cover" src="{{$profile->picture ? asset('storage/'.$profile->picture) : asset('basics/default.png')}}" 
                        alt="Profile photo">
                        <p class="ml-3 mb-0"><b>{{$profile->name}}</b><br>{{$profile->email}}</p>
                        @if (Auth::user()->id==$id)
                            <a href="/profile/edit" class="ml-auto"><button class="btn btn-primary" style="float:right">Edit Profile</button></a>
                        @endif
                    </div>
                    <div style="padding: 0 100px;">
                        {{$profile->bio}}
                    </div>
                </div>
                <div class="card-body p-0 pt-2 d-flex align-items-center">

                    <div class="mr-auto d-flex">
                        <div class="d-flex flex-column text-center px-2">
                            <span style="color: gray;">Follower</span>
                            <span style="font-size: 24px;"><b>{{count($profile->follower)}}</b></span>
                        </div>
                        <div class="d-flex flex-column text-center px-2 border-left">
                            <span style="color: gray;">Following</span>
                            <span style="font-size: 24px;"><b>{{count($profile->following)}}</b></span>
                        </div>
                    </div>

                    @if (Auth::user()->id!=$id)
                        @if (count($followed) > 0)
                        <form action="/profile/{{$id}}/unfollow" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-primary" style="float:right">Unfollow</button>
                        </form>
                        @else
                        <form action="/profile/{{$id}}/follow" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-primary" style="float:right">Follow</button>
                        </form>
                        @endif
                    @endif
                </div>
            </div>
            <hr>

            {{-- contoh postingan --}}
            @forelse ($posts as $post)
              @component('layouts.partials.postcard')
                @slot('post', $post)
              @endcomponent
            @empty
                <p>Tidak ada postingan</p>
            @endforelse
        </div>
        <div class="col-4">
            @include('layouts.partials.dongle')
        </div>
    </div>
@endsection
