@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-8">
            <div class="card p-2">
                <div class="card-header p-0">
                    <h4>Update your post</h4>
                    <p class="mb-1" style="font-size: 12px; color:gray;">Posted at {{date_format($post->updated_at, 'd-m-Y')}} | {{date_format($post->updated_at, 'H:i')}}</p>
                </div>
                <form action="/posts/{{$post->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                      <div class="card-body p-0 pt-2">
                        <div class="form-group mb-2">
                            <textarea rows="3" name="isi" id="isi" class="form-control" placeholder="What's on Your Mind?">{{$post->isi}}</textarea>
                        </div>
                        <div style="color: red; font-size:12px;">{{ $errors->first('isi') }}</div>
                        @if ($post->picture)
                        <img class="post d-block mx-auto rounded" src="{{asset('storage/'.$post->picture)}}" alt="Cannot load picture">
                        @endif
                        
                        <hr class="my-2">
                      </div>
                      <!-- /.card-body -->
                      <div class="card- footer d-flex justify-content-between">
                        <div>
                          <span class="mx-2"><i class="fas fa-image"></i> &nbsp; 
                            @if ($post->picture)
                                Update image : 
                            @else
                                Add image : 
                            @endif</span>
                          <input type="file" name="picture" id="picture">
                          {{-- <button id="imgbutton" class="opt btn btn-light btn-sm ml-1">Add picture &nbsp;<i class="fa fa-image"></i></button> --}}
                          {{-- <button class="opt btn btn-light btn-sm">Quotes &nbsp;  <i class="fas fa-quote-right"></i></button> --}}
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">Update &nbsp;<i class="fas fa-paper-plane pt-1 mr-1"></i></button>
                      </div>
                      <div style="color: red; font-size:12px;">{{ $errors->first('picture') }}</div>
                </form>
                  <!-- /.card-footer-->
            </div>
            <hr>
        </div>

        <div class="col-4">
          @include('layouts.partials.dongle')
        </div>
    </div>
  
@endsection