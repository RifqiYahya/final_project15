@extends('layouts.master')

@push('style')
    <style>
        .lk1 {
            color:gray;
        }

        .lk1:hover{
            color:darkslategray;
        }

        .lk2 {
            color: gray;
        }

        .lk2:hover{
            color:darkslategray;
            text-decoration: underline;
        }

        .lk3{
            color : palevioletred;
        }

        .lk3:hover{
            color : pink;
        }
    </style>
@endpush
@section('content')
<div class="row">
    <div class="col-8">

        {{-- contoh postingan --}}
        @component('layouts.partials.postcard')
            @slot('post', $post)
        @endcomponent

        {{-- Comments card --}}
        @component('layouts.partials.card')
            @slot('title')
                <form action="/comments" method="POST">
                    @csrf
                    <div class="form-group mb-0 d-flex">
                        <textarea class="form-control" name="isi" id="isi" rows="1" placeholder="Add comment here..."></textarea>
                        <button type='submit' class="btn btn-primary ml-2"><i class="fas fa-paper-plane"></i></button>
                        <input type="hidden" name="post_id" id="post_id" value={{$post->id}}>
                    </div>
                </form>
                <div class="mt-1" style="color: red; font-size:12px;">{{ $errors->first('isi') }}</div>
            @endslot
            @slot('body')
            {{-- Contoh komentar --}}
                @forelse ($post->comments as $comment)
                <div class="comments border-bottom mx-2 pb-1">
                    <div class="py-1">
                        <b class='d-block'>{{ $comment->user->name }}</b>
                        <span>{{ $comment->isi }}</span>
                    </div>
                    <div class="pl-1" style="font-size: 12px">
                        @php
                            $p = 0;
                        @endphp

                        @foreach ($comment->likes as $like)
                            @if ($like->user_id == Auth::user()->id)
                                <form class="d-inline" action="/comments/{{$comment->id}}/unlike" id="unlikecomment{{$comment->id}}" method="POST">
                                    @csrf
                                    <a class="lk3" href="#" onclick="
                                        document.getElementById('unlikecomment{{$comment->id}}').submit()">
                                        <i class="fas fa-heart" ></i>
                                    </a>
                                    <input type="hidden" name="post_id" value={{$post->id}}>
                                </form>
                                @break
                            @endif
                            @php
                                $p++;
                            @endphp
                        @endforeach

                        @if ($p == count($comment->likes))
                            <form class="d-inline" action="/comments/{{$comment->id}}/like" id="likecomment{{$comment->id}}" method="POST">
                                @csrf
                                <a class="lk2" href="#" onclick="
                                    document.getElementById('likecomment{{$comment->id}}').submit()">
                                    <i class="fas fa-heart" ></i>
                                </a>
                                <input type="hidden" name="post_id" value={{$post->id}}>
                            </form>
                        @endif
                        
                        <span>&nbsp; {{count($comment->likes) != 0 ? count($comment->likes) : null}}</span>
                        <a class="lk2" href="#">
                            like this comment
                        </a>

                        @if ($comment->user->id == Auth::user()->id)
                            <span>|</span>
                            <a class="lk2" href="#" 
                            onclick="
                                event.preventDefault();
                                document.getElementById('deletecomment{{$comment->id}}').submit();
                            ">
                                Delete
                            </a>
                            <form action="/comments/{{$comment->id}}" method="POST" id="deletecomment{{$comment->id}}">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="post_id" id="post_id" value={{$post->id}}>
                            </form>
                        @endif
                        
                    </div>
                </div>
                @empty
                    
                @endforelse    
            @endslot
        @endcomponent
    </div>
    <div class="col-4">
        @include('layouts.partials.dongle')
    </div>
</div>
@endsection