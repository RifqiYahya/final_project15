@extends('layouts.master')

@push('style')
    <style>
        .items:hover{
            opacity: 80%;
        }
    </style>
@endpush
@section('webtitle', 'MySocmed | Search')

@section('content')
    <div class="row">
        <div class="col-8">
            @component('layouts.partials.card')
            @slot('body')
                @foreach ($results as $result)
                <a href='/profile/{{$result->id}}' class="py-2 pl-3 d-flex align-items-center border-bottom items">
                    <img class='rounded-circle' 
                    style="width: 75px; height:75px; object-fit:cover" src="{{$result->picture ? asset('storage/'.$result->picture) : asset('basics/default.png')}}"
                     alt="Profile photo">
                    <p class="ml-3 mb-0" style='color: black;'><b>{{$result->name}}</b><br>{{$result->email}}</p>
                </a>
                @endforeach  
            @endslot
            @endcomponent
        </div>
        <div class="col-4">
            @include('layouts.partials.dongle')
        </div>
    </div>
  
@endsection