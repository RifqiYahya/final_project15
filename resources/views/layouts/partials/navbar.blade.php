<nav class="main-header navbar navbar-expand navbar-white navbar-light fixed-top">

    <div class="d-flex m-auto flex-wrap justify-content-between mcon">
      <div class="navbar-nav">
        <a href="/" style="color: #575757">  
          <h3>MySocmed</h3>
        </a>
      </div>

      <form class="form-inline" action="/search" method="GET">
        <div class="input-group input-group-sm">
          <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" name="keyword">
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      
  
      <!-- Right navbar links -->
      <ul class="navbar-nav">
        {{-- Profile dropdown menu --}}
        <li class="nav-item dropdown">
          <a href="#" class="nav-link dropdown-toggle" role='button' data-toggle="dropdown">
            <p class="mr-1 d-inline" style="color : #575757; margin-top : 2px">{{Auth::user()->name}}</p>
            <img class='rounded-circle' 
            style="width: 30px; height:30px; object-fit:cover" src="{{Auth::user()->picture ? asset('storage/'.Auth::user()->picture) : asset('basics/default.png')}}"
             alt="Profile pic">
          </a>
          <div class="dropdown-menu">
            <a href="/profile" class="dropdown-item"><i class="fas fa-user"></i>&nbsp; Profile</a>
            {{-- <a href="#" class="dropdown-item"><i class="fas fa-envelope"></i>&nbsp; Messages</a> --}}
            <a href="/profile/edit" class="dropdown-item"><i class="fas fa-edit"></i>&nbsp; Edit Profile</a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item" 
              onclick="
                event.preventDefault();
                document.getElementById('logout-form').submit();
            ">Log Out</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
          </div>
        </li>
      </ul>
    </div>
    <!-- SEARCH FORM -->
    
  </nav>