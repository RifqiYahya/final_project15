@component('layouts.partials.card')
    @slot('title')
        <div class="d-flex align-items-center pl-2 my-1">
            <a id="user-link" href="/profile/{{$post->user->id}}" style="color: black;">
                <img class="rounded-circle" 
                style="width: 30px; height:30px; object-fit:cover" src="{{$post->user->picture ? asset('storage/'.$post->user->picture) : asset('basics/default.png')}}" 
                alt="profile picture">
                <span class='ml-2'>{{ $post->user->name }}</span>
            </a>
            @if ($post->user->id == Auth::user()->id)
            <div class="dropdown ml-auto ">
                <button class="opt btn btn-light btn-sm" data-toggle="dropdown"><strong>...</strong></button>
                <div class="dropdown-menu">
                    <a href="posts/{{$post->id}}/edit" class="dropdown-item">Edit</a>
                    
                    <a href="#" class="dropdown-item" 
                    onclick="
                        event.preventDefault();
                        document.getElementById('deletepost{{$post->id}}').submit();
                        ">Delete</a>
                    <form action="posts/{{$post->id}}" method="POST" id="deletepost{{$post->id}}">
                        @csrf
                        @method('DELETE')
                    </form>
                </div>
            </div>
            @endif
        </div>
        
    @endslot

    @slot('body')
        <div class="container pt-2 px-2 ml-1">
        <p class="mb-1" style="font-size: 12px; color:gray;">Posted at {{date_format($post->updated_at, 'd-m-Y')}} | {{date_format($post->updated_at, 'H:i')}}</p>
        <p>{{ $post->isi }}</p>
        </div>           
        @if ($post->picture)
            <img class="post d-block mx-auto" src="{{ asset('storage/'.$post->picture) }}" alt="post picture">
        @else
            
        @endif
        
    @endslot

    @slot('footer')
        <div class="container d-flex align-items-center">

        @php
            $p = 0;
        @endphp

        @foreach ($post->likes as $like)
            @if ($like->user_id == Auth::user()->id)
                <form action="/posts/{{$post->id}}/unlike" method="POST">
                    @csrf
                    <button type="submit" class="opt btn btn-light rounded-circle" style="width: 40px; height:40px; padding: 8px 0">
                        <i style="font-size:24px; color:palevioletred;" class="fas fa-heart"></i></button>
                </form>
                @break
            @endif
            @php
                $p++;
            @endphp
        @endforeach

        @if ($p == count($post->likes))
            <form action="/posts/{{$post->id}}/like" method="POST">
                @csrf
                <button type="submit" class="opt btn btn-light rounded-circle" style="width: 40px; height:40px; padding: 8px 0">
                    <i style="font-size:24px;" class="fas fa-heart"></i></button>
            </form>
        @endif

        <span style="font-size: 20px;" class="mx-1">{{count($post->likes)}}</span>
        <a href="/posts/{{$post->id}}">
            <button class="opt btn btn-light rounded-circle" style="width: 40px; height:40px; padding: 8px 0">
                <i style="font-size:24px" class="fas fa-comments"></i></button>
        </a>
        <span style="font-size: 20px;" class="mx-1">{{count($post->comments)}}</span>
        </div>
    @endslot
@endcomponent