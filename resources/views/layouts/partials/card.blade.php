<div class="card">
  <div class="card-header p-2" >
    {{ isset($title) ? $title : null }}

    <div class="card-tools">
      {{ isset($topmenu) ? $topmenu : null }}
      {{-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fas fa-minus"></i></button> --}}
      {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fas fa-times"></i></button> --}}
    </div>
  </div>
    <div class="card-body p-0 pb-1">
      {{ isset($body) ? $body : null}}
    </div>
    <!-- /.card-body -->
    <div class="card- footer mb-1">
      {{ isset($footer) ? $footer : null}}
    </div>
    <!-- /.card-footer-->
</div>