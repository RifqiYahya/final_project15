
<div class="container position-fixed">
    <div class="fix py-4 pl-3 d-flex align-items-center">
        <img class='rounded-circle' 
        style="width: 75px; height:75px; object-fit:cover" src="{{Auth::user()->picture ? asset('storage/'.Auth::user()->picture) : asset('basics/default.png')}}" 
        alt="Profile photo">
        <p class="ml-3 mb-0"><b>{{Auth::user()->name}}</b><br>{{Auth::user()->email}}</p>
    </div>
    <div class=" fix d-flex flex-column p-3 border-top" style="opacity: 70%;">
        <div class="float-right d-none d-sm-block">
            <b>Version</b> 3.0.5
        </div>
          <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> 
          All rights reserved.
    </div>
</div>

  {{-- <footer class="main-footer fixed-bottom">
    
  </footer> --}}